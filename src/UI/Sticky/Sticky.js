
import React, { Component } from 'react';

import {WarningFilled} from '@ant-design/icons';
import {Button} from 'antd';
import './Sticky.css';


export default class Sticky extends Component {

    state={
        stickyOpen :true
    }

    
    removeSticky =()=>{
        this.setState({stickyOpen:false})
    }

    render() {


        let stickyCss = null ;
        this.state.stickyOpen ? stickyCss='Sticky' : stickyCss='DisplayNone' ;



        return (
            
                <div className={stickyCss} >
                    <div className='Info'>
                        <p className='rtl'><WarningFilled/>تلگرام پشتیبانی:@Miladjfd-شماره تماس پشتیبانی:09359949473-روزها و ساعات کاری سایت 7روز هفته از 10صبح الی 8 شب
                        <Button className='btn-shop float-left mt-1' onClick={this.removeSticky}>بستن</Button>
                        </p>
                </div>
                
            </div>
        )
    }
}

