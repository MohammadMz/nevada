import React, { Component } from 'react'
import { connect } from 'react-redux';
import ShopTable from '../../components/ShopTable/ShopTable';


export class ShopingCart extends Component {

    componentWillMount(){
        if(this.props.orders.length === 0){
            this.setState({emptyCart:true});
        }
    }
    state={
        emptyCart:false,

    }



    render() {

        const orderArray=this.props.orders;

        return (
            <div>

                {!this.state.emptyCart ?<ShopTable list={orderArray} 
                
                totPrice = {this.props.totalPrice}
                
                changeInput={(record,value)=>this.props.changeInput(record,value)} /> :<div><h1 className='rtl text-center mt-10 text-gray-600 ' style={{fontSize:'25px'}}>سبد خرید شما خالیست</h1></div>
 }
                
            </div>
        )
    }
}
 
const mapStateToProps = (state) => {
    
    
    return {
    orders :state.orderList,
    totalPrice  : state.totalPrice

    
}}

export default connect(mapStateToProps)(ShopingCart)
