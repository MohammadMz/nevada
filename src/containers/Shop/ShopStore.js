import React, { Component } from 'react';
import { connect } from 'react-redux';
import Images from '../../UI/ImagesUrl/ImagesUrl';
import {Button,Pagination} from 'antd';
import * as actions from '../../store/actions/actions';

export class ShopStore extends Component {

    state = {
        minValue : 0 ,
        maxValue : 4 ,
        allItems : null,
        loadItems :false,
        pageSize : 4
    }



    componentDidMount(){


        this.concatItems();

    }

    concatItems(){
        const games = this.props.games ;
        const gifts = this.props.giftCards ;
        const services = this.props.services ;
        const inApps = this.props.inAppPurchases;
        const allProducts = games.concat(gifts,services,inApps);
        //console.log(allProducts);
        this.setState({allItems:allProducts,loadItems:true});

        //console.log(this.state.allItems);
    


    }




    pageChangeHandler=(pageNumber,pageSize)=>{
       // nextValue = this.state.value + 4 ;'
           this.setState({minValue:(pageNumber-1)* pageSize,maxValue:pageSize*pageNumber});
    }




    render() {
                let items= null;
                if(this.state.loadItems){
                items=this.state.allItems.slice(this.state.minValue,this.state.maxValue).map(item=>{
                    return<div className='m-4 w-64' key={item.Id} >
    
                    <img 
                    src={Images[item.name]}
                    className='rounded Imgs'
                    alt={item.name}/>
                    <div className='text-center mt-2'>
                    <p>{item.name}</p>
                    <p>{item.price}</p>
                    <Button className='btn-shop'
                    onClick={()=>this.props.addToCart(item.Id,item.type)}
                    >افزودن به سبد خرید

                    </Button>
                    </div>


                            </div>

                })

            }



        return (
            <div className='mx-auto container'>

                <h1 className='Head rtl'>آخرین محصولات</h1>
                
                <div className='container mx-auto'>
                    <div className='grid md:grid-cols-3 gap-4'>
                        {items}
                    </div>
                </div>
                <Pagination className='text-center'
                onChange={(pageNmber,pageSize)=>this.pageChangeHandler(pageNmber,pageSize)}
                defaultCurrent={1}
                pageSize={this.state.pageSize}
                total={16}/> 
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
    games : state.products.games ,
    giftCards : state.products.giftCards,
    inAppPurchases :state.products.inAppPurchase,
    services :state.products.services

    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        addToCart:(ID,Type)=>dispatch(actions.addCart(ID,Type))

    };
    
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopStore)
