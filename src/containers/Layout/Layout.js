import React, { Component } from 'react';
import Navigation from '../../components/NavigationItems/NavigatinItems';
import Footer from '../../components/Footer/Footer';
import {BackTop} from 'antd';
import Sticky from '../../UI/Sticky/Sticky';
export class Layout extends Component {





    render() {
        return (

            <React.Fragment>
                <BackTop/>
                <Navigation/>

                <main style={{minHeight:'550px'}}>
                    {this.props.children}
                </main>
                
                <Footer/>
                <Sticky/>
                
            </React.Fragment>
        )
    }
}

export default Layout
