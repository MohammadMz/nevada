import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';

export class MobilehackFullPost extends Component {


    render() {


        const mobile = [...this.props.mobileArray];
        let mobilePost = mobile.find((item)=> item.id === this.props.match.params.id);
        return (
            <div className='PostContainer'>
                <h1 className='Head'>{mobilePost.header}</h1>
                <img src={mobilePost.imgUrl} alt='PostImage' className='rounded-b my-5' style={{width:'80%'}}/>
                <ReactPlayer url={mobilePost.videoLink} controls={true}/>
                <p className='FontPSize'>{mobilePost.details}</p>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        mobileArray : state.mobile 
    }
    
}


export default connect(mapStateToProps)(MobilehackFullPost);
