import React, { Component } from 'react';
import { connect } from 'react-redux';
import TrainPosts from '../../components/TrainPosts/trainPosts';
import {Link} from 'react-router-dom';


export class MobileHacks extends Component {



    render() {
        const mobile = [...this.props.mobileArray];
        let mobilePosts = null ;
        
        mobilePosts=mobile.map(mobilePost=>{
            return(
            <Link to={'/cellphone/'+ mobilePost.id}key={mobilePost.id}
            >
              <TrainPosts 
                    
                     header={mobilePost.header}
                     imgUrl ={mobilePost.imgUrl}
                     videoLink={mobilePost.videoLink}
                     details = {mobilePost.details}
                     record = {mobilePost}
                     />
            </Link>
            )
         })

        return (

            <div className='MyContainer mx-auto rtl'>
                <h1 className='Head mr-5 py-5'>آموزش های بازیهای موبایل</h1>

                <div className=' grid md:grid-cols-2 lg:grid-cols-4 align-content-center  gap-4'>
                    {mobilePosts}

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mobileArray : state.mobile

    }
    
}

export default connect(mapStateToProps)(MobileHacks);