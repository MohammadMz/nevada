import React, { Component } from 'react';
import {Form,Input,Button } from 'antd';

export class OrderTrace extends Component {
    render() {
        return (
            <div className='Box container mx-auto rtl mt-5'>
                <h1 className='Head mb-5'>پیگیری سفارش</h1>
                <p className='FontPSize mt-5'>برای رهگیری سفارشتان شماره سفارش و ایمیلی که درهنگام ثبت سفارش وارد کردید را در این قسمت وارد و کلید رهگیری را فشار دهید.</p>
                <Form labelCol={{span:4}}>
                    <Form.Item label='شماره سفارش'  rules={[{required:true}]}>

                    <Input/>
                    </Form.Item>
                    <Form.Item label='ایمیل صورتحساب '  rules={[{required:true}]}>

                    <Input/>
                    </Form.Item>
                    <Button htmlType='submit'></Button>







                </Form>
            </div>
        )
    }
}

export default OrderTrace;
