import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';
import './WarcraftFullPost.css';

export class WarcraftFullPost extends Component {



    render() {


        const warcrafts = [...this.props.warcraftArray];

        let warPost = warcrafts.find((item)=> item.id === this.props.match.params.id);

        console.log(warPost);
        return (
            <div className='PostContainer'>
                <h1 className='Head'>{warPost.header}</h1>
                <img src={warPost.imgUrl} alt='PostImage' className='rounded-b my-5' style={{width:'80%'}}/>
                <ReactPlayer url={warPost.videoLink} controls='true'/>
                <p className='FontPSize'>{warPost.details}</p>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        warcraftArray : state.warCrafts 
    }
    
}


export default connect(mapStateToProps)(WarcraftFullPost)
