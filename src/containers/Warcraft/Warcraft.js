import React, { Component } from 'react';
import { connect } from 'react-redux';
import TrainPosts from '../../components/TrainPosts/trainPosts';
import {Link} from 'react-router-dom';


export class Warcraft extends Component {
    componentDidMount(){
        console.log(this.props);
    }



    render() {
        const warCrafts = [...this.props.warCraftArray];
        let warposts = null ;
        
        warposts=warCrafts.map(warPost=>{
            return(
            <Link to={'/world-of-warcraft/'+ warPost.id}key={warPost.id}
            >
              <TrainPosts 
                    
                     header={warPost.header}
                     imgUrl ={warPost.imgUrl}
                     videoLink={warPost.videoLink}
                     details = {warPost.details}
                     record = {warPost}
                     />
            </Link>
            )
         })

        return (

            <div className='MyContainer mx-auto rtl'>
                <h1 className='Head mr-5 py-5'>آموزش های World Of Warcraft:</h1>

                <div className=' grid md:grid-cols-2 lg:grid-cols-4 align-content-center  gap-4'>
                    {warposts}

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        warCraftArray : state.warCrafts

    }
    
}

export default connect(mapStateToProps)(Warcraft);