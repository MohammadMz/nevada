import React, { Component } from 'react';
import Carousel from '../../components/Carousel/Carousel';
import MainTab from '../../containers/MainTab/MainTab';
import Images from '../../UI/ImagesUrl/ImagesUrl';
import {connect} from 'react-redux';
import BestSellers from '../../components/BestSellers/BestSellers';
export class Main extends Component {


    render() {
        const games=this.props.games;
        const giftCards=this.props.giftCards;
        const services=this.props.services;
        const inAppPurchases =this.props.inAppPurchases;

        let allItems=[];
        games.map(game=>allItems.push(game)
        );
        giftCards.map(gift=>allItems.push(gift));
        services.map(service=>allItems.push(service));
        inAppPurchases.map(inApp=>allItems.push(inApp));
        const sortedItems = allItems.slice().sort((a,b)=>b.itemsSold-a.itemsSold);
        const bestSellers = sortedItems.slice(0,12);
        //console.log(bestSellers);
        return (
            <React.Fragment>
                <div className='container mx-auto'>
                    <Carousel/>
                    <MainTab/>
                    <img src={Images.Banner1} alt='ads' className='m-10 mx-auto' />
                    <BestSellers top12={bestSellers}/>

                </div>
                
            </React.Fragment>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        games : state.products.games ,
        giftCards : state.products.giftCards,
        inAppPurchases :state.products.inAppPurchase,
        services :state.products.services

    }
    
    
}


export default connect(mapStateToProps)(Main);
