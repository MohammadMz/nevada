import React, { Component } from 'react';
import{Tabs,Button} from 'antd';
import {connect} from 'react-redux';
import ImageUrl from '../../UI/ImagesUrl/ImagesUrl';
import  './MainTab.css';
import * as actions from '../../store/actions/actions';
const {TabPane} = Tabs;

 export class MainTab extends Component {

    componentDidMount(){
        //console.log(this.props.games);
    
    
    
    }

    render() {

     const gamesArray=this.props.games ;
     const sortednewItems =gamesArray.slice().sort((a,b)=> b.date - a.date);
     const newItems = sortednewItems.slice(0,4);
        let gameInfo = null ;
         gameInfo = newItems.map(game=>{
             //console.log(ImageUrl[game.name]);
            return(
                    <div key={game.Id} >

                        <img 
                        src={ImageUrl[game.name]}
                        className='rounded Imgs'
                        alt={game.name}/>
                        <div className='text-center mt-2'>
                        <p>{game.name}</p>
                        <p>{game.price}</p>
                        <Button className='btn-shop'
                        onClick={()=>this.props.addToCart(game.Id,game.type)}
                        
                        >افزودن به سبد خرید
                        </Button>
                        </div>
                    </div>

            )
        })


        //rendering giftCards 


        const giftCards = this.props.giftCards ;
        let allGifts = null ;
        allGifts =giftCards.map(gift=>{

            return<div key={gift.Id} >

            <img 
            src={ImageUrl[gift.name]}
            className='rounded Imgs'
            alt={gift.name}/>
            <div className='text-center mt-2'>
            <p>{gift.name}</p>
            <p>{gift.price}</p>
            <Button className='btn-shop' 
            onClick={()=>{this.props.addToCart(gift.Id,gift.type)}
            
            }>افزودن به سبد خرید</Button>
            </div>
        </div>


        })


        //rendering inApppurchases 
        const inApps = this.props.inAppPurchases;
        let inAppPurchases= null ;
        inAppPurchases = inApps.map(inApp=>{
            return<div key={inApp.Id} >

            <img 
            src={ImageUrl[inApp.name]}
            className='rounded Imgs'
            alt={inApp.name}/>
            <div className='text-center mt-2'>
            <p>{inApp.name}</p>
        <p>{inApp.minPrice}-{inApp.maxPrice}</p>
            <Button className='btn-shop' onClick={()=>this.buyList(inApp.Id,inApp.type)}>افزودن به سبد خرید</Button>
            </div>
        </div>


        })

        //rendering services
         const allServices = this.props.services;
         let services = null;
         services= allServices.map(service=>{
            return<div key={service.Id} >

            <img 
            src={ImageUrl[service.name]}
            className='rounded Imgs'
            alt={service.name}/>
            <div className='text-center mt-2'>
            <p>{service.name}</p>
            
            <p>{service.minPrice}-{service.maxPrice}</p>
            <Button className='btn-shop'                      onClick={()=>{this.props.addToCart(service.Id,service.type)}}

            
            >افزودن به سبد خرید</Button>
            </div>
            </div>
            
         })
         //rendering offers 

         let inSales =gamesArray.filter(game=>game.inSale=== true);
         inSales = inSales.slice(0,4);

         let appsInSales = null ;
         appsInSales = inSales.map(inSale=>{
            return<div key={inSale.Id} >

            <img 
            src={ImageUrl[inSale.name]}
            className='rounded Imgs'
            alt={inSale.name}/>
            <div className='text-center mt-2'>
            <p>{inSale.name}</p>
            <p><span style={{textDecoration: 'line-through'}}>{inSale.price}</span>قیمت قبل</p>
            <p>{inSale.inSalePrice}قیمت حراجی</p>
            <Button className='btn-shop' onClick={()=>this.props.addToCart(inSale.Id,inSale.type) }>افزودن به سبد خرید</Button>
            </div>
            </div>


         })


        return (
            
            <Tabs type='card' className='  rtl text-center MainTab' defaultActiveKey='10'>


                <TabPane  tab='جدیدترین بازیها'  key='games'>
                <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4' >

                  {gameInfo}
                    
                </div>
                </TabPane>
                <TabPane  tab='گیفت کارت'  key='giftCards'>
                <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4' >

                  {allGifts}
                    
                </div>
                </TabPane>
                <TabPane  tab='آیتم های داخل بازی'  key='inAppPurchases'>
                <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4' >

                  {inAppPurchases}
                    
                </div>
                </TabPane>
                <TabPane  tab='خدمات بازی'  key='services'>
                <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4' >

                  {services}
                    
                </div>
                </TabPane>
                <TabPane  tab='تخفیف های جدید '  key='inSale'>
                <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4' >

                  {appsInSales}
                    
                </div>
                </TabPane>
                {/* <NewItems tab=' ترین games' games={newItems} key='10' ></NewItems>
                <NewItems tab='جدید ترین ' games={newItems} key='2' ></NewItems>
                <TabPane key='1' tab='جدیدترین محصولات' games={newItems}>
                    what the hell
                
                </TabPane>
                <TabPane key='2' tab='گیفت کارت'>

                </TabPane>
                <TabPane key='3' tab='آیتم های داخل بازی'>

                </TabPane>
                <TabPane key='4' tab='خدمات بازی'>

                </TabPane>
                <TabPane key='5' tab='تخفیف های جدید'>

                </TabPane> */}
                
            </Tabs>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        games : state.products.games ,
        giftCards : state.products.giftCards,
        inAppPurchases :state.products.inAppPurchase,
        services :state.products.services

    }
    

}
    

    const mapDispatchToProps = dispatch => {
        return {
            addToCart:(ID,Type)=>dispatch(actions.addCart(ID,Type))
    
            
        }
    }
    
    


export default connect(mapStateToProps,mapDispatchToProps)(MainTab);

