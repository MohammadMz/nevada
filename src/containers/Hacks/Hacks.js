import React, { Component } from 'react';
import { connect } from 'react-redux';
import TrainPosts from '../../components/TrainPosts/trainPosts';
import {Link} from 'react-router-dom';


export class Hacks extends Component {



    render() {
        const hacks = [...this.props.hacksArray];
        let hackPosts = null ;
        
        hackPosts=hacks.map(hackPost=>{
            return(
            <Link to={'/hacks/'+ hackPost.id}key={hackPost.id}
            >
              <TrainPosts 
                    
                     header={hackPost.header}
                     imgUrl ={hackPost.imgUrl}
                     videoLink={hackPost.videoLink}
                     details = {hackPost.details}
                     record = {hackPost}
                     />
            </Link>
            )
         })

        return (

            <div className='MyContainer mx-auto rtl'>
                <h1 className='Head mr-5 py-5'> ترفندها</h1>

                <div className=' grid md:grid-cols-2 lg:grid-cols-4 align-content-center  gap-4'>
                    {hackPosts}

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        hacksArray : state.hacks

    }
    
}

export default connect(mapStateToProps)(Hacks);