import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';

export class HackFullPost extends Component {


    render() {


        const hacks = [...this.props.hacksArray];
        let hackPost = hacks.find((item)=> item.id === this.props.match.params.id);
        return (
            <div className='PostContainer'>
                <h1 className='Head'>{hackPost.header}</h1>
                <img src={hackPost.imgUrl} alt='PostImage' className='rounded-b my-5' style={{width:'80%'}}/>
                <ReactPlayer url={hackPost.videoLink} controls={true}/>
                <p className='FontPSize'>{hackPost.details}</p>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        hacksArray : state.hacks 
    }
    
}


export default connect(mapStateToProps)(HackFullPost);
