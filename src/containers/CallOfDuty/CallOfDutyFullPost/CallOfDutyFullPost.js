import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';

export class CallOfDutyFullPost extends Component {



    render() {


        const callOfDuty = [...this.props.callOfDutyArray];

        let callOfDutyPost = callOfDuty.find((item)=> item.id === this.props.match.params.id);
        return (
            <div className='PostContainer'>
                <h1 className='Head'>{callOfDutyPost.header}</h1>
                <img src={callOfDutyPost.imgUrl} alt='PostImage' className='rounded-b my-5' style={{width:'80%'}}/>
                <ReactPlayer url={callOfDutyPost.videoLink} controls='true'/>
                <p className='FontPSize'>{callOfDutyPost.details}</p>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        callOfDutyArray : state.callOfDuty 
    }
    
}


export default connect(mapStateToProps)(CallOfDutyFullPost);
