import React, { Component } from 'react';
import { connect } from 'react-redux';
import TrainPosts from '../../components/TrainPosts/trainPosts';
import {Link} from 'react-router-dom';


export class CallOfDuty extends Component {



    render() {
        const callOfDuty = [...this.props.callOfDutyArray];
        let callOfDutyposts = null ;
        
        callOfDutyposts=callOfDuty.map(callPost=>{
            return(
            <Link to={'/call-of-duty/'+ callPost.id}key={callPost.id}
            >
              <TrainPosts 
                    
                     header={callPost.header}
                     imgUrl ={callPost.imgUrl}
                     videoLink={callPost.videoLink}
                     details = {callPost.details}
                     record = {callPost}
                     />
            </Link>
            )
         })

        return (

            <div className='MyContainer mx-auto rtl'>
                <h1 className='Head mr-5 py-5'>آموزش های Call Of Duty:</h1>

                <div className=' grid md:grid-cols-2 lg:grid-cols-4 align-content-center  gap-4'>
                    {callOfDutyposts}

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        callOfDutyArray : state.callOfDuty

    }
    
}

export default connect(mapStateToProps)(CallOfDuty);