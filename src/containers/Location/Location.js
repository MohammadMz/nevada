import React,{Component} from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import './Location.css';
class Location extends Component  {
    state = {
        lat: 51.505,
        lng: -0.09,
        zoom: 25,
    }




    
    
    render(){

        const position = [this.state.lat, this.state.lng]

    return (
        <div id='mapid'>
            <Map center={position}  >
                <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                <Marker position={position}>
                <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
                </Marker>
            </Map>
            
        </div>
    )
}
}

export default Location;
