import React, { Component } from 'react';
import {Form,Input,Button} from 'antd';
import Location from '../../containers/Location/Location';
const {TextArea} = Input ;

export class ContactUs extends Component {


    render() {
        return (
            <React.Fragment>
                <div className='Box rtl mx-auto container mt-20'>
                    <h1 className='Head mb-2'>تماس با ما:</h1>
                    <div className='Acounts'>
                        <p></p>
                    </div>

                    <Form labelCol={{span:4}}>
                        

                        
                                <Form.Item label='نام شما ' name="username" rules={[{required:true}]}>

                                    <Input/>
                            

                                </Form.Item>
                        
                    


                        
                            
                        <Form.Item label='شماره تماس' 
                        name="telNumber"
                        rules={[{
                            type:'number'
                        }]}
                        
                        >
                            <Input/>
                        </Form.Item>
                            
                        
                        <Form.Item label='ایمیل' name='email' rules={[{required:true ,type:'email'}]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item label='موضوع ' 
                        name='subject'>

                            <Input/>
                        </Form.Item>
                        <Form.Item label='پیام شما ' 
                        name='feedback'>

                            <TextArea/>
                        </Form.Item>

                        <Button type='primary' htmlType='submit'>
                            ارسال
                        </Button>













                    </Form>















                    <p className='FontPSize'>نشانی ایمیل شما منتشر نخواهد شد. بخش‌های موردنیاز علامت‌گذاری شده‌اند *</p>
                </div>
                {/* <div id='mapid'>
                    <Location/>
                </div> */}
                
            </React.Fragment>
        )
    }
}

export default ContactUs;
