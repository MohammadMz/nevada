import React from 'react';
import './styles/app.css';
import {BrowserRouter,Route} from 'react-router-dom';
import Layout from './containers/Layout/Layout';
import Main from './containers/Main/Main';
import ShoppingCart from './containers/ShopingCart/ShopingCart';
import About from './components/About/About';
import ContactUs from './containers/ContactUs/ContactUs';
import OrderTrace from './containers/OrderTrace/OrderTrace';
import ShopStore from './containers/Shop/ShopStore';
import Warcraft from './containers/Warcraft/Warcraft';
import WarcraftFullPost from './containers/Warcraft/WarcraftFullPost/WarcraftFullPost';
import CallOfDuty from './containers/CallOfDuty/CallOfDuty';
import CallOfDutyFullPost from './containers/CallOfDuty/CallOfDutyFullPost/CallOfDutyFullPost';
import Mobile from './containers/MobileHacks/MobieHacks';

import MobileHacksFullPosts from './containers/MobileHacks/MobileHacksFullpost/MobileHacksFullpost';
import Hacks from './containers/Hacks/Hacks';
import HacksFullPost from './containers/Hacks/HacksFullPost/HacksFullPost';


function App() {

  return (<React.Fragment>
      <BrowserRouter>
        <Layout>
          <Route path={'/'} exact component={Main} />
          {/* <Route path={'/Newest'} exact component={NewestItems}/> */}
          <Route path={'/shopping-cart'} exact component={ShoppingCart}/>
          <Route path={'/about-us'} exact component={About}/>
          <Route path={'/contact-us'} exact component={ContactUs}/>
          <Route path={'/order-trace'} exact component={OrderTrace}/>
          <Route path={'/shop-store'} exact component={ShopStore}/>
          <Route path={'/world-of-warcraft'} exact component={Warcraft}/>
          <Route path={'/world-of-warcraft/:id'} component={WarcraftFullPost}/>
          <Route path={'/call-of-duty'} exact component={CallOfDuty}/>
          <Route path={'/call-of-duty/:id'} component={CallOfDutyFullPost}/>
          <Route path={'/cellphone'} exact component={Mobile}/>
          <Route path={'/cellphone/:id'} component={MobileHacksFullPosts}/>
          <Route path={'/hacks'} exact component={Hacks}/>
          <Route path={'/hacks/:id'} component={HacksFullPost}/>

          

        </Layout>




      </BrowserRouter>
    </React.Fragment>
    
  );
}

export default App;
