
import * as actionTypes from '../actions/actionTypes';
import Images from '../../UI/ImagesUrl/ImagesUrl';

const createGift=(ID,name,qty,value)=>{
    const dollorPrice = 15000 ;
    return {
        Id:ID,
        type:'giftCards',
        name:name,
        qty:qty,
        price :(value * dollorPrice),
        value:value,
        itemsSold :0,
        cartQty : 0 ,
        itemsPrice :0


    }
}
const createGame=(ID,name,qty,price,inSale,inSalePrice,date)=>{
    return {
        Id:ID,
        type:'games',
        name:name,
        qty:qty,
        price :price ,
        inSale : inSale ,
        inSalePrice : inSalePrice,
        date:date,
        itemsSold :0,
        cartQty :0,
        itemsPrice : 0


    }
}

 const createTrainpost = (ID,postName,Header,img,videoLink,details)=>{
    return {
        id : ID ,
        name : postName,
        imgUrl : img ,
        videoLink : videoLink ,
        header : Header ,
        details : details
    }
}



const initialState = {
                        products:{
        games:[

            createGame('000','Forza-Horizon 4',25,50000,false,50000,new Date('2020-05-20'),25),
            createGame('001','GTA',125,10000,false,10000,new Date('2019-05-18'),35),
            createGame('002','Call Of Duty',5,10000,true,7000,new Date('2020-02-02'),150),
            createGame('003','Most Wanted',25,40000,true,25000,new Date('2005-02-26'),235),
            createGame('004','Red Dead Redemption 2',10,630000,true,423000,new Date('2016-01-13'),180),
            createGame('005','Minecraft Windows 10',25,250000,true,205000,new Date('2018-05-01'),250) ,
            createGame('006','Valorant Beta',10,68000,true,25800,new Date('2020-01-15'),20)

            
            // {
            // Id:'000',
            // name:'Forza-Horizon 4',
            // qty:'25',
            //         price:'50000',},
            //     {
            //         Id:'001',
            //         name:'GTA 4 Online',
            //         qty:'100',
            //         price:'300000',},
            //     {
            //         Id:'002',
            //         name:'Driver',
            //         qty:'125',
            //         price:'10000',},
            //     {
            //         Id:'003',
            //         name:'Most wanted',
            //         qty:'25',
            //         price:'40000',}
                ],
            giftCards :
                        [
                            createGift('015','STEAM WALLET CARD $5',200,5,300)
                            ,
                            createGift('016','STEAM WALLET CARD $10',100,10,100),
                            createGift('017','STEAM WALLET CARD $15',80,15,40),
                            createGift('018','STEAM WALLET CARD $50',10,50,10)


                        ]   
                        ,
            inAppPurchase : 
                            [
                                {
                                    Id : '019' ,
                                    type:'inAppPurchase',
                                    name:'Dota 2 Arcana',
                                    minPrice:296000,
                                    price:460000,
                                    itemsSold:270 ,
                                    qty:2,
                                    cartQty : 0,
                                    itemsPrice : 0
                                }
                            ]
                        ,
            services :      
                            [   
                                {
                                Id :'020',
                                type :'services',
                                name :'Shark Card',
                                price :11000 ,
                                maxPrice :200000,
                                inSale : false ,
                                salePrice : 0,
                                itemsSold :120,
                                qty:100 ,
                                cartQty : 0,
                                itemsPrice : 0


                                },
                                {
                                Id :'021',
                                type :'services',
                                name :'تغییر واحد پول استیم',
                                price :2000 ,
                                maxPrice :12000,
                                inSale : false ,
                                salePrice : 0,
                                itemsSold:200,
                                qty:100,
                                cartQty:0,
                                itemsPrice : 0

                                },
                                {
                                Id :'022',
                                type :'services',
                                name :'سرویس های داخل بازی',
                                price :0 ,
                                maxPrice :0,
                               inSale : false ,
                                salePrice : 0,
                                itemsSold:150,
                                qty:100,
                                cartQty :0,
                                itemsPrice : 0

                                },
                                {
                                Id :'023',
                                type :'services',
                                name :'EA ORIGIN Acesss Basic',
                                price :35000 ,
                                maxPrice :68000,
                                inSale : false ,
                                salePrice : 35000,
                                itemsSold:170,
                                qty:100 ,
                                cartQty : 0,
                                itemsPrice : 0

                                }

                            ]
            
            
                        },
                        orderList:[                                
],
                        totalPrice : 0,
                        warCrafts : [
                            createTrainpost('111','WowBeginner','آموزش مبتدی بازی World of Warcraft یا WOW',Images['WowBeginner'],'https://aspb2.cdn.asset.aparat.com/aparat-video/ea14c3c468cb0fd917ba567fcdeb79944657082-360p.mp4','در این ویدیو با مهارت های ابتدایی بازی World Of Warcraft آشنا میشوید.'),
                            createTrainpost('112','WowLegion','آموزش آنلاین بازی کردن WOW Legion 7.1.5',Images['WowLegion'],'https://aspb2.cdn.asset.aparat.com/aparat-video/ea14c3c468cb0fd917ba567fcdeb79944657082-360p.mp4','آموزش ساده از آنلاین بازی کردن WOW 7.1.5'
                            ),  
                            createTrainpost('113','WoWAddons','آموزش راحت بازی کردن WOW با Addons جدید',Images['WowAddons'],'https://ads.cdn.asset.aparat.com/aparat-video/fb3fa98cae20e60cf548ed1b79ee265922074152-1080p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImQ5ODYyNGEzZTEwMjJhOWNlM2ZmNzY3YjA4ZDdmNjE5IiwiZXhwIjoxNTkyNzc0ODkxLCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.zAcE1uUd99D6zFT4l_ZXra9h5GRGkQJpC93T7P1PDHY','آموزش ساده از Addons بازی WOW 7.1.5'),
                            createTrainpost('114','WowHero1','آموزش هیرو های بازی World of Warcraft پارت 1',Images['WowHero1'],'https://aspb13.cdn.asset.aparat.com/aparat-video/89cbdaf6afa1f1cdbeb2d111f7da46514620336-540p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImEyOGFiYWIzYjUwZjgzMWYyOGU1ZDViMDdjOTAyYjI2IiwiZXhwIjoxNTkyODYxODQ0LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.Mms_ZaV8Ny3c8sSMlFv7WlaU4f-5jmBZWBNzZ7RazDM','آموزش هیرو های بازی World of Warcraft پارت 1'),
                            createTrainpost('115','WowHero2','آموزش هیرو های بازی World of Warcraft پارت 2                    ',Images['WowHero2'],'https://aspb3.cdn.asset.aparat.com/aparat-video/5fb486113ec8b17bf9c87973d4b0544f4627069-540p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjMxMGVjYjJjMzZjZmI2NTNlZTdhZTg1MzA5NjVmNzQ3IiwiZXhwIjoxNTkyNzc1NzE5LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.5CHWOdlm_ty0fOjgDUH4ppY5hrv37bGH9LSEgm3kdJQ','در این ویدیو آموزش کامل هیرو های این بازی داده شده'),
                            createTrainpost('116','WowPatch','آموزش پچ آنلاین بازی 3.3.5 World of Warcraft یا WOW',Images['WowPatch'],'https://as1.cdn.asset.aparat.com/aparat-video/8e0d5efa71e3b5bb9d4f193f325122384639108-720p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImM2OTYzZjY2MTUzNTU1ZTE0MmYzZDk4OWVhYTI2NzBmIiwiZXhwIjoxNTkyNzc1ODc5LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.kBtXGAjoMGUCqJ94uJhcz5eVGVyHbZBk_28kpfBEElA','آموزش ساده از آنلاین بازی کردن WOW 3.3.5'),
                            createTrainpost('117','WowOnline','آموزش آنلاین بازی 4.3.4 World of Warcraft یا WOW',Images['WowOnline'],'https://as7.cdn.asset.aparat.com/aparat-video/d408e8bd0570e75a1bd11482cf011d074617663-720p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjUyNDFhZmY2NDY2ZjU1ODUxNTMyYTYyOTBkNGY1YzEyIiwiZXhwIjoxNTkyNzc2MjAyLCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.KJFxkYsZ4rwZJ82tymX_oi_GhNwnQcbj-cGcbVpVG9k','آموزش ساده از آنلاین بازی کردن WOW 4.3.4'),
                            createTrainpost('118','WowJob','آموزش شغل یا Job در بازی World of Warcraft یا WOW',Images['WowJob'],'https://aspb1.cdn.asset.aparat.com/aparat-video/49aa5e1141cdb682b34708397227b7304661329-484p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImY2OGRkOGY5ZTVhYWNkYjhkYjE2YzE4Mzc4ZjNmN2Y3IiwiZXhwIjoxNTkyODYyNzQ4LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.OoUc-4mgY89uuFlraP4Wg_nqMG0WzeH4nZC5l5Q7FfM','در این ویدئو آموزش اولیه مربوط به درآمدزایی توسط شغل ها داده شده')


                            
                        ],
                        callOfDuty :[
                            createTrainpost('200','CallOfDutyMW2','آموزش آنلاین بازی کردن Call of Duty MW2',Images['CallOfDutyMW2'],'https://as4.cdn.asset.aparat.com/aparat-video/0fabb7283b7750007e511af8d3a50a177304262-480p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjFmMWE4NjlhZjcwYjgzYjg3YmM0NTM2NGQ3NTQyMGY4IiwiZXhwIjoxNTkyODYyMzU1LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.xU65O9UuZbq1AptaBa7QHl_QMRENTKvQSSbCBkTq0cg','آموزش ساده از مولتی پلیر بازی MW2')
                        ],
                        mobile:[
                            createTrainpost('300','MobileLegends','گیم پلی بازی Mobile Legends',Images['MobileLegends'],'https://as1.cdn.asset.aparat.com/aparat-video/d9cfe4b37e2c89b4ad3b6b7e18e31a8a7204034-360p.apt?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjhkNTMyZWU1M2VhN2NlNGJlOWNjZjc2ZjdjMzc0NmYzIiwiZXhwIjoxNTkyOTQ4ODc4LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.WGVUdra_yzTPtR-TINUhq88XMKzikdLSplc2wkE8yxE','در این ویدئو گیمپلی ساده از بازی Mobile Legends نمایش داده شده .')
                        ],
                        hacks:[
                            createTrainpost('400','WinBoost','بهینه سازی ویندوز برای بازی',Images['WinBoost'],'https://aspb15.cdn.asset.aparat.com/aparat-video/f06c193cfc75deb2bea3090738f717537281129-360p.apt/chunk.m3u8?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImYwMjZlNmZmNDJkODA4NTVhZTk5N2JjMGQxYjk1N2UwIiwiZXhwIjoxNTkyOTQ5NDU4LCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.mr2s_AZwHqKXysRmwTZ8WwJ1lHhvd93jfywMaDpDMdM','افزایش FPS بازی ها در این آموزش')
                        ]

            }

 const reducer = (state = initialState, action) => {
    switch (action.type) {
        // add items to the shopingcart and order list
        case actionTypes.ADD_TO_CART:
            // coping array without refrence to work with immutabally
            let orders=[...state.orderList];
            let allProducts={...state.products};
            //find the type of product  for example :games,service,giftcard
             let targetedItemType = allProducts[action.productType]
            //find the selecteditem type and check for the id
             let targetedItem = targetedItemType.find(item=>item.Id===action.productID);
            
             let newOrder ;
             // check if the item exists and subtract the quantity if exsisted 
            if (targetedItem.qty > 0){
                targetedItem.qty -- ;
                targetedItem.itemsSold ++ ;


            //if the item had already been in the shoping cart and orde list just plus the quantity instead of creating new table row 
                if(targetedItem.cartQty > 0){  
        
                    targetedItem.cartQty ++;
                    newOrder =[...orders];
                    targetedItem.itemsPrice = targetedItem.price * targetedItem.cartQty;
                
                    


                }

                // if the item hadnt been on the shopping cart create a new row in the table and add it to the orderlist
                else{

                    newOrder =orders.concat(targetedItem);
                    targetedItem.cartQty ++ ;
                    targetedItem.itemsPrice = targetedItem.price * targetedItem.cartQty;
                    


                }

            }


            // if the item dosnt exist on the story do nothing and log "out of supply"
            else {
                console.log('موجودی ندارد');
                newOrder = [...state.orderList]
            }

            
            // update the state immutably
            return{
                ...state,
                orderList:newOrder,
                totalPrice :state.totalPrice + targetedItem.price
            }


        //this action remove the item in the shopping cart and update the database
        case actionTypes.DELETE_FROM_CART:
            //coping the state and create a new variable to copy data from the database and create a new object in order to work immutably
            let allProducts2={...state.products}
            let orders2=[...state.orderList];

            //find the type of the item to retrive it from products array
            const targetedItemType2 =allProducts2[action.item.type];

            // check the clicked item id and check wethr the item exists in the database or not
            const targetedItem2=targetedItemType2.find(item=>item.Id===action.item.key);
            // since the item is deleted we just update the quantity again by adding the quantity in the shopingcart to the database 
            //also update the itemssold property and put zreo in the item in the shopingcart propery because it has been remove from shopping cart
            targetedItem2.qty += action.item.cartQty;
            targetedItem2.itemsSold -= action.item.cartQty;
            targetedItem2.cartQty = 0 ;
            

            //removing the item from the order list and use filter method insteaad of pop method because filter method returning new array and doesnt change the initial array 
            let updatedArray = orders2.filter(item=>item.Id!==action.item.key);
        
            


            // update the state
            return{
                ...state,
                orderList:updatedArray ,
                totalPrice :state.totalPrice - targetedItem2.itemsPrice

            }



            // this action control the changes to the quantity of the item in the shopingcaart
            case actionTypes.ADD_QUANTITY :
            // getting the products from the database and coping it to the new variable 
                const allProducts3 = {...state.products};
            //find the type of the selected item
                let targetedItemType3 = allProducts3[action.productType];

            //find the selected item
                let targetedItem3 = targetedItemType3.find(item=>item.Id===action.productID);

            //getting the inputvalue from input and update the number of the items in the soppingcart
                  targetedItem3.cartQty = action.productValue ;
            //check wethear the value has changed or not 
            
            //value property compares the previous value and current value to check the change
                  const value = action.productValue - action.prevValue ;
                  if(value !== 0){
                      //value has been increased or decreased
                      //update the number of the products in the store
                      targetedItem3.qty -= value ;
                      targetedItem3.itemsPrice = action.productValue * targetedItem3.price    
                  }
                  else {
                      // value hasnt change so we just return from the action
                      //console.log('value hasnt changed');
                      return ;
                  }

                //update the state 
                 return{
                     ...state,
                     products : allProducts3 ,
                     totalPrice :state.totalPrice + targetedItem3.price

                 }
                
    }
    return state ;
}


export default reducer;