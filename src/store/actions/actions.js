import * as actionTypes from './actionTypes';

export const addCart = (ID,Type)=>{


    return{
    type : actionTypes.ADD_TO_CART,
    productID : ID ,
    productType :Type
    }
}


export const removeCart = (record)=> {
    return {
        type : actionTypes.DELETE_FROM_CART ,
        item : record
        }
}



export const addQuantity = (record,value,prevValue)=>{


    return {
        type : actionTypes.ADD_QUANTITY ,
        productType :record.type ,
        productID : record.key ,
        productValue :value,
        prevValue : prevValue 
    }

}