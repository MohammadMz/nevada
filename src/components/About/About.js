import React from 'react';
import './About.css';

const about = () => {
    return (
        <div className='container  rtl mt-10 Box mx-auto'>
            <h1 className='Head'>درباره ما</h1>
            <p className='FontPSize'>فروشگاه نوادا گیمینگ , در راستای تحقق ارزش های بنیادی بازی های رایانه , از سال 1395 فعالیت خود را در زمینه آموزش های بازی های مختلف توسط کانال آپارات خود آغاز کرد . اکنون که 3 سال از آغاز فعالیت ما گذشته است , تصمیم گرفتیم به هم میهنان عزیزمان در سرتاسر نقاط کشور خدمت رسانی کنیم و به وسیله بازی های رایانه ای سالم , محیطی امن به همراه آرامش را به خانه های عزیزانمان بیاوریم . امید است شما عزیزان ما را در این راه همراهی کنید .</p>
            <p><small className='FontPSmall'>باتشکر , مدیریت سایت</small></p>
            
            <p><small className='FontPSmall'>MiladNevada</small></p>
            
        </div>
    )
}

export default about;
