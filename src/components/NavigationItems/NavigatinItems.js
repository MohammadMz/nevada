import React from 'react'; 
import { Menu} from 'antd';
import {  BankOutlined, DownOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import './NavigationItems.css';

import {Link} from 'react-router-dom';


const {SubMenu} = Menu ;


let greyStyle = {
    color: '#3d3d3d'
}

let menuStyle = {
    backgroundColor: '#fa541c',
    color: '#3d3d3d',
    padding: '8px'
}
const allNavItems =[
    {
        key:'home',
        link : '/',
        hasIcon : true,
        icon : <BankOutlined style={greyStyle}/> ,
        hasValue:false,
        submenu:false ,
        subItems:[]
    }
    ,
    {
        key :'shop-store',
        link:'/shop-store',
        hasIcon:false,
        hasValue : true,
        value:'فروشگاه' ,
        submenu :false
    }
    ,
    {
        hasIcon:true,
        icon : <DownOutlined/>,
        hasValue : true,
        value:'آموزشها',
        submenu :true ,
        subItems:[
            {
            key: 'world-of-warcraft',
            value:'World Of Warcraft' ,
            link:'/world-of-warcraft'
            },
            
            {
            key: 'call-of-duty',
            value:'Call Of Duty' ,
            link:'/call-of-duty'
            },
            
            {
            key: 'cellphone',
            value:'موبایل' ,
            link:'/cellphone'
            },
            
            {
            key: 'hacks',
            value:'ترفند ها' ,
            link:'/hacks'
            }

            ]
    },
    {
        key :'charge',
        link:'/charge',
        hasIcon:false,
        hasValue : true,
        value:'شارژ مستقیم استیم' ,
        submenu :false
    },
    {
        key :'order-trace',
        link:'/order-trace',
        hasIcon:false,
        hasValue : true,
        value:'پیگیری سفارش' ,
        submenu :false
    },
    {
        key :'shopping-cart',
        link:'/shopping-cart',
        hasIcon:false,
        hasValue : true,
        value:'سبد خرید' ,
        submenu :false
    },
    {
        key :'about-us',
        link:'/about-us',
        hasIcon:false,
        hasValue : true,
        value:'درباره ما ' ,
        submenu :false
    },
    {
        key :'contact-us',
        link:'/contact-us',
        hasIcon:false,
        hasValue : true,
        value: 'تماس با ما',
        submenu :false
    }




    

]
const header=(props)=>{

    let navItems =null ;
    navItems =allNavItems.map(navItem=>{
        if (!navItem.submenu){
         
            if(navItem.hasValue && navItem.hasIcon){
                //value and icon both
                return(<Menu.Item key={navItem.key} >
                    <Link to={navItem.link}>
                    {navItem.icon}{navItem.value}
                    </Link>
                    </Menu.Item>);

            } else 
                {
                    if (navItem.hasValue){
                        return (<Menu.Item key={navItem.key} >
                            <Link style={greyStyle} to={navItem.link}>
                            {navItem.value}
                            </Link>
                            </Menu.Item>);
    
                        

                    }else if (navItem.hasIcon){
                        return(<Menu.Item key={navItem.key} >
                        <Link to={navItem.link}>
                        {navItem.icon}
                        </Link>
                        </Menu.Item>);
    
                    }else {
                        //both were false 
                        return true;

                    }


                }



        }
            else {

                // Item has submenu
                let subItemsCompo = null ;
                subItemsCompo = navItem.subItems.map(subItem=> {
                    return(<Menu.Item key={subItem.key}
                    >
                        <Link to={subItem.link}>{subItem.value}</Link>


                    </Menu.Item>);

                });

                //End of the subItems map 
                return(<SubMenu key={navItem.value}

                title={navItem.value}

                icon={navItem.icon}


            >{subItemsCompo}</SubMenu>) ;



            

            }
    })

return(
    <div>

        <Menu mode="horizontal" className='Navbar' style={menuStyle}>
                {navItems}
        </Menu>
    </div>


);
}

export default header;