import React, { Component } from 'react';
import 'antd/dist/antd.css';
import {Carousel} from 'antd'
import Thieves from '../../assets/r.jpg';
import ForzaHorison from '../../assets/Forza-Horizon-4-Pedal-to-the-Metal1.jpg';
import  GTA from '../../assets/e413843feaaec0f7cd3a5d6cae2a4535.jpg';
import './Carousel.css';
import {Link} from 'react-router-dom';


export default class Carousel1 extends Component{
    render(){
        const carouselStyle ={
            width: '100%',
            textAlign: 'center',
            height: '60vh',
            margin: '20px auto ',
            borderRadius: '25px',
            


        }


        const imgStyle = {
            width: '100%',
            height: '60vh',
            borderRadius: '25px',

        }

        const carSetting = {
            autoplay: true ,
            autoplaySpeed : 7000 ,
            fade : true ,
            dots :true ,
            dotPosition : 'bottom',
            slidesToShow : 1 ,
            slidesToScroll : 1,
            pauseOnHover : true 



        }
    
    return (
        

        <div  className='container mx-auto '>
            <Carousel {...carSetting}  style={carouselStyle}  >
                <div  className='carItem'>
                <img alt='thieves' src={Thieves} style={imgStyle}/>  
                <h3  className='carText'>بازی اورجینال دزدان دریایی 30 هزار تومان</h3>
                </div>
                <div className='carItem'>
                <img alt='forza-horizon' src={ForzaHorison} style={imgStyle}/>    
                <h3 className='carText'>فورزا 4 اولتیمیت 30 هزار تومان</h3>
                </div>
                <div className='carItem'>
                <img alt='forza-horizon' src={GTA} style={imgStyle}/>    
                <Link className='link' to='/GTA'>مشاهده سرویس ها</Link>
                </div>
                
            </Carousel>
            
        </div>    
        
        
    );
    }
}