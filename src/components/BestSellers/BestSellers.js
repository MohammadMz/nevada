import React,{Component} from 'react';
import Images from '../../UI/ImagesUrl/ImagesUrl';
import {Button} from 'antd';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/actions';
 class BestSellers extends  Component {


    render()
    {
        let bestSellers = this.props.top12.map(top=>{
            return<div key={top.Id} >
    
                        <img 
                        src={Images[top.name]}
                        className='rounded Imgs'
                        alt={top.name}/>
                        <div className='text-center mt-2'>
                        <p>{top.name}</p>
                        <p>{top.price}</p>
                        <Button className='btn-shop'
                        onClick={()=>this.props.addToCart(top.Id,top.type)}
                        >افزودن به سبد خرید
    
                        </Button>
                        </div>
    
    
                                </div>
        })
    
    

    
    return (

        <React.Fragment>
            <h1 className='rtl text-gray-700 text-xl'>پرفروشترین محصولات</h1>
            
            <div className='grid md:grid-cols-3 lg:grid-cols-4 gap-4'>
            {bestSellers}  
            </div>

        </React.Fragment>
    )
}}


const mapDispatchToProps = (dispatch) => {
    
    return{
        addToCart:(ID,Type)=>dispatch(actions.addCart(ID,Type))



    }

}


export default  connect(null,mapDispatchToProps)(BestSellers);
