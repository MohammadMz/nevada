import React from 'react';
import {Card,Button} from 'antd';

const {Meta} = Card ;

const trainPosts = (props) => {
    return (
        <div className='rtl m-4 mx-5 text-center'>
            <Card
            hoverable
            style={{width:400 , height: 350}}
            cover={<img alt={props.name} style={{height: 200 , width:400}} src={props.imgUrl}/>}>
            <Meta title={props.header}/>
            <Button className='mt-6'>بیشتر بخوانید</Button>
            </Card>
        </div>
    )
}

export default trainPosts;
