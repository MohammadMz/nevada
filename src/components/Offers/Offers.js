import React, { useEffect } from 'react';
import {Tabs} from 'antd';
import ImageUrl from '../../UI/ImagesUrl/ImagesUrl';
import Offer from './offer/offer';

const {TabPane}=Tabs ;


 const Offers = (props) => {

    // useEffect(()=>{
    //     console.log(props.games)
    //     console.log(games);
    // },[]);

     let games=null ;

     games=props.games.map(game=>{
        // console.log(ImageUrl[game.name]);
        // console.log(game.name);
    
    return(
        <div className='bg-blue-200' key={game.Id} >
            <img src={ImageUrl[game.name]} alt={game.name}  />
            <p>{game.name}</p>
            <p>{game.price}</p>
            

        </div>
        )

    })

    return (
        <React.Fragment>
        <Tabs>

        <TabPane tab={props.tab} key={props.key}>
            {games}
        </TabPane>
        </Tabs>
        </React.Fragment>
    )
}

export default Offers ;