import React from 'react';
import './Footer.css';
import {Link} from 'react-router-dom';
import {PhoneFilled,MailFilled,} from '@ant-design/icons';
import Images from '../../UI/ImagesUrl/ImagesUrl';
import {Button} from 'antd';

 const footer = () => {




    return (
        <div style={{backgroundColor:'#610b00'}} className='mt-10'>
            <div className='Footer grid md:grid-cols-3 gap-4 rtl container mx-auto py-10 '>
                <div>
                    <h1 className='Footer-Header'>
                    <PhoneFilled className='FootIcon'/>
                    راههای تماس با ما</h1>
                    <p><PhoneFilled className='FootIcon'/>شماره تماس پشتیبانی:09359949473</p>
                    <p><MailFilled className='FootIcon'/>Miladjfd@gmail.com</p>
                    <p style={{margin:'0 10px'}}>تلگرام:    
                        <Link to='telegram.me/Miladjfd' style={{color:'#fff'}}>Miladjfd</Link>
                    </p>
                </div>
                <div>
                    <h1 className='Footer-Header'>سایر کانال های ما</h1>
                    <a href='https://steamcommunity.com/id/miladnevada'> <img  src={Images.CandyBar} className='pr-2' alt='candybar'/>
                    </a>
                    <br/>
                    <a href='Aparat.com/NevadaGaming' style={{color:'#fff'}}>Aparat : Aparat.com/NevadaGaming</a>
                    <br/>
                    <a href='telegram.com/NevadaGaming' style={{color:'#fff'}}>Telegram : @NevadaGaming</a>


                </div>
                <div>
                    
                    <h1 className='Footer-Header'>ارسال پول بصورت دستی</h1>
                    <p>قبل از ارسال با پشتیبانی هماهنگ کنید</p>
                    <Button block className='btn-shop'>درگاه دستی</Button>

                

                </div>
            </div>
            </div>

        
    )
}


export default footer;