import React,{Component} from 'react';
import { connect } from 'react-redux';
import {Table,InputNumber, Button} from 'antd';
import {CloseCircleFilled} from '@ant-design/icons';
import Images from '../../UI/ImagesUrl/ImagesUrl';
import * as actions from '../../store/actions/actions';

class ShopTable extends Component{

    state = {
        inputValue:1,
        record:{},
        disableInput : false
        
    }

    componentDidUpdate(prevProps,prevState){


        if(prevState.inputValue !== this.state.inputValue){
        console.log(this.state.inputValue);
         this.props.changeInput(this.state.record,this.state.inputValue,prevState.inputValue);
    

          if(this.state.record.qty <= 0){
              console.log('why not');
              this.setState({disableInput : true})

          }
        }
    }
    
    render(){



    const columns = [


        {
            title : '' ,
            dataIndex : 'img',
            key : 'img',
            render:(text,record)=> <img style={{width:'50px',height:'50px'}} src={record.img} alt={record.name}/>
        }
        ,
    {
        title :'محصول',
        dataIndex : 'product',
        key : 'product'
    },
        {
            title :'قیمت' ,
            dataIndex :'price' ,
            key : 'price'
        },

        {
            title : 'تعداد' ,
            dataIndex :'quantity' ,
            key : 'quantity',
            render : (text,record)=> <InputNumber  defaultValue={record.cartQty}
            value = {record.cartQty}
        
             onChange ={(event)=>{
                 this.setState({inputValue:event,record:record})
                 
                
                }
            
                } 
        
            disabled ={this.state.disableInput}
            
            />
        },

        {
            title : 'مجموع' ,
            dataIndex : 'sum',
            key : 'sum'  }  ,
        {
            title :'حذف ازسبد خرید',
            dataIndex :'',
            key : 'btn-Close',
            render : (text,record)=> <CloseCircleFilled onClick={()=>this.props.deleteItem(record)} />
    
        },
    
    
]






    const list = this.props.list ;
    let data =[];
    data = list.map(item=>{
    

        return{
            img : Images[item.name] ,
            key : item.Id ,
            type : item.type,
            product: item.name ,
            price : item.price ,
            cartQty : item.cartQty  ,
            sum : item.itemsPrice ,
            qty :item.qty
            



        }


    })
            

        
    
    
    // console.log(data);
    //console.log(list);
    return (
        <div className='container mx-auto mt-10 '>
            <Table  columns={columns} style={{direction: 'rtl'}}  dataSource={data} />
            <div className='w-48 bg-green-600 h-16 float-right rounded'>
                <h1 className='text-white float-right mr-5 mt-5'>{this.props.totPrice}: جمع کل سفارش</h1>
            </div>
            <div className=' h-10  w-16'>
                <Button className=' float-left '>پرداخت</Button>
            </div>

        </div>
    )


    }}


    const mapDispatchToProps = (dispatch)=>{



        return {
            changeInput : (record,value,prevValue)=>dispatch(actions.addQuantity(record,value,prevValue)),


            deleteItem : (record)=>dispatch(actions.removeCart(record))
        
        
        
            
        }
        
    }
    
export default  connect(null,mapDispatchToProps)(ShopTable);
