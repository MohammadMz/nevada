import React from 'react';
import {Breadcrumb} from 'antd';
import {Link,Route,BrowserRouter} from 'react-router-dom';
import NewItems from '../../containers/Items/NewItems';

const BreadCrumb = (props) => {
    return (
        <div className="container mx-auto text-center " style={{direction: 'rtl'}}>
            
                <Route path='/Newest' exact component={NewItems}/>
            <Breadcrumb separator="">


                <Breadcrumb.Item>
                <Link to='/Newest'> جدیدترین محصولات</Link> 
                </Breadcrumb.Item>
                <Breadcrumb.Separator/>
                <Breadcrumb.Item >گیفت کارت</Breadcrumb.Item>
                <Breadcrumb.Separator />
                <Breadcrumb.Item href="">آیتم های داخل بازی </Breadcrumb.Item>
                <Breadcrumb.Separator />
                <Breadcrumb.Item>خدمات بازی</Breadcrumb.Item>
                <Breadcrumb.Separator/>
                <Breadcrumb.Item>تخفیف های جدید</Breadcrumb.Item>
        </Breadcrumb>


            
        </div>
    )
}

export default BreadCrumb
